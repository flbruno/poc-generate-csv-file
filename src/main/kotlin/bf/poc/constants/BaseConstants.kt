package bf.poc.constants

class BaseConstants {

    companion object {

        // Message
        const val START_READING_CSV_FILE = "Starting csv file reading"
        const val EXECUTE_SUCCESSFULLY_JOB = "Performed job successfully"
        const val ERROR_READ_CSV_FILE = "Error read csv file"
        const val ERROR_GENERATE_CSV_FILE = "Error generate csv file"

        // Status Process
        const val PROCESSED = "PROCESSED"
        const val CONCLUDED = "CONCLUDED"
    }
}