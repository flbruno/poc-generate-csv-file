package bf.poc.repository

import bf.poc.entity.Accumulation
import bf.poc.entity.Process
import io.micronaut.data.annotation.Query
import io.micronaut.data.annotation.Repository
import io.micronaut.data.jpa.repository.JpaRepository
import java.util.*

@Repository
interface TransactionRepository : JpaRepository<Process, Long> {

    @Query("FROM Process p WHERE p.status = :pending")
    fun findByStatus(pending: String?): List<Process>

}