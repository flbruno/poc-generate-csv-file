package bf.poc.job;

import java.lang.System;

@kotlin.Metadata(mv = {1, 4, 2}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\b\u0007\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\b\u0010\u0007\u001a\u00020\bH\u0007R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\t"}, d2 = {"Lbf/poc/job/MainJob;", "", "service", "Lbf/poc/service/CsvReaderFile;", "(Lbf/poc/service/CsvReaderFile;)V", "path", "", "processCsvFile", "", "project"})
@javax.inject.Singleton()
public final class MainJob {
    private java.lang.String path = "/home/bcostaf/work/iupp/poc-020/project/src/main/resources/mockdata020file.csv";
    private final bf.poc.service.CsvReaderFile service = null;
    
    @io.micronaut.scheduling.annotation.Scheduled(cron = "${main.job.cron.value}")
    public final void processCsvFile() {
    }
    
    public MainJob(@org.jetbrains.annotations.NotNull()
    bf.poc.service.CsvReaderFile service) {
        super();
    }
}