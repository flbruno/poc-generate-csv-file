package bf.poc.entity;

import java.lang.System;

@kotlin.Metadata(mv = {1, 4, 2}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0012\b\u0007\u0018\u00002\u00020\u0001B\'\u0012\b\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\b\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\tR\u001e\u0010\u0004\u001a\u00020\u00058\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\n\u0010\u000b\"\u0004\b\f\u0010\rR\"\u0010\u0002\u001a\u0004\u0018\u00010\u00038\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0010\n\u0002\u0010\u0012\u001a\u0004\b\u000e\u0010\u000f\"\u0004\b\u0010\u0010\u0011R \u0010\b\u001a\u0004\u0018\u00010\u00058\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0013\u0010\u000b\"\u0004\b\u0014\u0010\rR\u001e\u0010\u0006\u001a\u00020\u00078\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0015\u0010\u0016\"\u0004\b\u0017\u0010\u0018\u00a8\u0006\u0019"}, d2 = {"Lbf/poc/entity/Accumulation;", "", "id", "", "cpf", "", "transactionAmount", "Ljava/math/BigDecimal;", "processDate", "(Ljava/lang/Long;Ljava/lang/String;Ljava/math/BigDecimal;Ljava/lang/String;)V", "getCpf", "()Ljava/lang/String;", "setCpf", "(Ljava/lang/String;)V", "getId", "()Ljava/lang/Long;", "setId", "(Ljava/lang/Long;)V", "Ljava/lang/Long;", "getProcessDate", "setProcessDate", "getTransactionAmount", "()Ljava/math/BigDecimal;", "setTransactionAmount", "(Ljava/math/BigDecimal;)V", "project"})
@io.micronaut.core.annotation.Introspected()
@javax.persistence.Entity()
public final class Accumulation {
    @org.jetbrains.annotations.Nullable()
    @javax.persistence.Column()
    private java.lang.String processDate;
    @org.jetbrains.annotations.Nullable()
    @javax.persistence.SequenceGenerator(name = "seq_generator_ac", sequenceName = "SEQ_DATABASE_AC", allocationSize = 1)
    @javax.persistence.GeneratedValue(generator = "seq_generator_ac", strategy = javax.persistence.GenerationType.SEQUENCE)
    @javax.persistence.Id()
    private java.lang.Long id;
    @org.jetbrains.annotations.NotNull()
    @javax.persistence.Column(name = "cpf", nullable = false, unique = false)
    private java.lang.String cpf;
    @org.jetbrains.annotations.NotNull()
    @javax.persistence.Column()
    private java.math.BigDecimal transactionAmount;
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getProcessDate() {
        return null;
    }
    
    public final void setProcessDate(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Long getId() {
        return null;
    }
    
    public final void setId(@org.jetbrains.annotations.Nullable()
    java.lang.Long p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getCpf() {
        return null;
    }
    
    public final void setCpf(@org.jetbrains.annotations.NotNull()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.math.BigDecimal getTransactionAmount() {
        return null;
    }
    
    public final void setTransactionAmount(@org.jetbrains.annotations.NotNull()
    java.math.BigDecimal p0) {
    }
    
    public Accumulation(@org.jetbrains.annotations.Nullable()
    java.lang.Long id, @org.jetbrains.annotations.NotNull()
    java.lang.String cpf, @org.jetbrains.annotations.NotNull()
    java.math.BigDecimal transactionAmount, @org.jetbrains.annotations.NotNull()
    java.lang.String processDate) {
        super();
    }
}