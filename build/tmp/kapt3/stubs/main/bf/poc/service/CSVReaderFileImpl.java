package bf.poc.service;

import java.lang.System;

@kotlin.Metadata(mv = {1, 4, 2}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000F\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010!\n\u0002\b\u0002\b\u0007\u0018\u0000 \u001a2\u00020\u0001:\u0001\u001aB\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u0018\u0010\u000b\u001a\u00020\b2\u0006\u0010\f\u001a\u00020\b2\u0006\u0010\r\u001a\u00020\nH\u0002J\u0010\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\nH\u0016J\u0010\u0010\u0011\u001a\u00020\u000f2\u0006\u0010\u0012\u001a\u00020\u0013H\u0002J\u0018\u0010\u0014\u001a\u00020\u000f2\u0006\u0010\u0012\u001a\u00020\u00132\u0006\u0010\u0015\u001a\u00020\u0016H\u0002J\u0016\u0010\u0017\u001a\u00020\u000f2\f\u0010\u0018\u001a\b\u0012\u0004\u0012\u00020\u00130\u0019H\u0002R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001b"}, d2 = {"Lbf/poc/service/CSVReaderFileImpl;", "Lbf/poc/service/CsvReaderFile;", "transactionRepository", "Lbf/poc/repository/TransactionRepository;", "accumulationRepository", "Lbf/poc/repository/AccumulationRepository;", "(Lbf/poc/repository/TransactionRepository;Lbf/poc/repository/AccumulationRepository;)V", "divisionFactor", "Ljava/math/BigDecimal;", "processDate", "", "pointCalculator", "value", "cpf", "readCsv", "", "path", "saveProcess", "process", "Lbf/poc/entity/Process;", "updateValueAccumulation", "accumulationData", "Lbf/poc/entity/Accumulation;", "validatorAccumulationToProcess", "processList", "", "Companion", "project"})
@javax.inject.Singleton()
public final class CSVReaderFileImpl implements bf.poc.service.CsvReaderFile {
    private final java.lang.String processDate = null;
    private final java.math.BigDecimal divisionFactor = null;
    private final bf.poc.repository.TransactionRepository transactionRepository = null;
    private final bf.poc.repository.AccumulationRepository accumulationRepository = null;
    private static final org.slf4j.Logger LOG = null;
    @org.jetbrains.annotations.NotNull()
    public static final bf.poc.service.CSVReaderFileImpl.Companion Companion = null;
    
    @java.lang.Override()
    public void readCsv(@org.jetbrains.annotations.NotNull()
    java.lang.String path) {
    }
    
    private final void validatorAccumulationToProcess(java.util.List<bf.poc.entity.Process> processList) {
    }
    
    private final void saveProcess(bf.poc.entity.Process process) {
    }
    
    private final void updateValueAccumulation(bf.poc.entity.Process process, bf.poc.entity.Accumulation accumulationData) {
    }
    
    /**
     * Method points calculator
     *
     * @param value Transaction value
     * @return Return point calculator by transaction
     */
    private final java.math.BigDecimal pointCalculator(java.math.BigDecimal value, java.lang.String cpf) {
        return null;
    }
    
    public CSVReaderFileImpl(@org.jetbrains.annotations.NotNull()
    bf.poc.repository.TransactionRepository transactionRepository, @org.jetbrains.annotations.NotNull()
    bf.poc.repository.AccumulationRepository accumulationRepository) {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 4, 2}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u0016\u0010\u0003\u001a\n \u0005*\u0004\u0018\u00010\u00040\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0006"}, d2 = {"Lbf/poc/service/CSVReaderFileImpl$Companion;", "", "()V", "LOG", "Lorg/slf4j/Logger;", "kotlin.jvm.PlatformType", "project"})
    public static final class Companion {
        
        private Companion() {
            super();
        }
    }
}