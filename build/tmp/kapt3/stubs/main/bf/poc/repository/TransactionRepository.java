package bf.poc.repository;

import java.lang.System;

@kotlin.Metadata(mv = {1, 4, 2}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\t\n\u0000\n\u0002\u0010 \n\u0000\n\u0002\u0010\u000e\n\u0000\bg\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001J\u0018\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00020\u00052\b\u0010\u0006\u001a\u0004\u0018\u00010\u0007H\'\u00a8\u0006\b"}, d2 = {"Lbf/poc/repository/TransactionRepository;", "Lio/micronaut/data/jpa/repository/JpaRepository;", "Lbf/poc/entity/Process;", "", "findByStatus", "", "pending", "", "project"})
@io.micronaut.data.annotation.Repository()
public abstract interface TransactionRepository extends io.micronaut.data.jpa.repository.JpaRepository<bf.poc.entity.Process, java.lang.Long> {
    
    @org.jetbrains.annotations.NotNull()
    @io.micronaut.data.annotation.Query(value = "FROM Process p WHERE p.status = :pending")
    public abstract java.util.List<bf.poc.entity.Process> findByStatus(@org.jetbrains.annotations.Nullable()
    java.lang.String pending);
}